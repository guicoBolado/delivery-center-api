const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const path = require('path');
const routes = require('./routes');
const PORT = process.env.PORT || 3333
const bodyParser = require('body-parser');

const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const MONGO_URL = 'mongodb+srv://admin:gui12345@cluster0.djggb.mongodb.net/delivery-center?retryWrites=true&w=majority';


mongoose.connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Delivery API',
            description: '',
            contact: {},
            servers: ["http://localhost:3333"]
        }
    },
    apis: ["./routes.js"]
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.listen(PORT, () => console.log(`Listening on ${ PORT }`))

app.use(bodyParser.urlencoded({extended: false}));//Parse application/xxx-www-url form encoded
app.use(bodyParser.json()); //parse application/json

app.use(cors());
app.use(express.json());
app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads')));
app.use(routes);


// req.query = Acessar query params (para filtros)
// req.params = Aceesar route params
//req.body = Acessar corpo da requisição