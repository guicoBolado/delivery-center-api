const Order = require('../models/Order');

module.exports = {
    //GET
    async show(req,res) {
        const order = await Order.find();
        return res.json(order);
    },
    //GET BY ID
    async index(req, res) {
        try {
            const order = await Order.findById(req.params._id);
            return res.json(order)
        } catch (error) {
            return res.status(400).json({msg:'ID invalido.', error });
        }
    },

    //POST
    async store(req, res) {
        try {
            const { store, externalReference, amount, deliveryFee, customer, address, items, payments } = req.body;
            const order = await Order.create({
                store, 
                externalReference, 
                amount, 
                deliveryFee, 
                customer, 
                address, 
                items, 
                payments
            })
            return res.status(200).json({order: order, msg:'Pedido enviado com sucesso.'});

        } catch (error) {
            return res.status(400).json({msg:'Todos os itens são obrigatório.', error });
        }

    },
    //DELETE
    async delete(req, res) {
        try {
            await Order.findByIdAndDelete(req.params._id);
            return res.status(200).json({msg:'O Pedido foi apagado com sucesso!' });
        } catch (error) {
            return res.status(400).json({msg:'Erro ao deletar Pedido' });
        }
    },
    //PUT
    async put(req, res) {
        try {
            await Order.findByIdAndUpdate(req.params._id, req.body, {useFindAndModify: false});
            return res.status(200).json({msg:'Pedido editado com sucesso', order: res.body });
        } catch (error) {
            return res.status(400).json({msg:'ERRO ao editar Pedido', error: error}, );
        }
    }

}
