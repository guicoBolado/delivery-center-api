const express = require('express');
const bodyParser = require('body-parser');

const OrderController = require('./controllers/OrderController');

const routes = express.Router();


/**
 * @swagger
 * /order:
 * get:
 *  description: Use to request all projects
 *  responses:
 *      '200':
 *          description: A successful response
 */


routes.get('/order', OrderController.show);
routes.get('/order/:_id', OrderController.index);
routes.put('/order/:_id', OrderController.put);
routes.delete('/order/:_id', OrderController.delete);
routes.post('/order', OrderController.store);

module.exports = routes;

