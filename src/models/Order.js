const mongoose = require('mongoose');


const customer = new mongoose.Schema({ name: String });
const address = new mongoose.Schema({ number: String, neighborhood: String, complement: String, city: String, state: String, street: String });
const items = new mongoose.Schema({ name: String, amount: String, quantity: Number,  note: String });
const payments = new mongoose.Schema({ method:  {type: String, enum: ['CREDIT', 'DEBIT', 'ONLINE']}, amount: Number });



const OrderSchema = new mongoose.Schema({
    store: {
        type: String,
        required: [false, 'store é obrigatório.'],
    },
    externalReference: {
        type: String,
        required: [false, 'externalReference é obrigatório.'],
    },
    amount: {
        type: Number,
        required: [false, 'amount é obrigatório.'],
    }, 
    deliveryFee: {
        type: Number,
        required: [false, 'deliveryFee é obrigatório.'],
    },  
    customer: {
        type: [customer],
        required: [false, 'customer é obrigatório.'],
    },  
    address: {
        type: [address],
        required: [false, 'address é obrigatório.'],
    },     
    
    items: {
        type: [items],
        required: [false, 'items é obrigatório.'],
    },  
    payments: {
        type: [payments],
        required: [false, 'payments é obrigatório.'],
    },     
});


module.exports = mongoose.model('Order', OrderSchema)


// _id: String!
// store: OrderStoreEnum!
// externalReference: String
// amount: Int
// deliveryFee: Int
// customer: [
//     _id,
//     name
// ]
// address: [
//     _id: String!
//     number: String
//     neighborhood: String
//     complement: String
//     city: String
//     state: String
//     street: String
// ]
// items: [
//     _id: String!
//     name: String
//     amount: String
//     quantity: Int
//     note: String
// ]
// payments: [
//     _id: String!
//     method: PaymentMethodEnum!
//     amount: Int!
// ]


